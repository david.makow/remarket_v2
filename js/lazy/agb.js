webpackJsonp([83],{

/***/ 771:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(16);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(28);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactRouter = __webpack_require__(206);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var AGB = function AGB() {
  return _react2.default.createElement(
    'div',
    { className: 'container' },
    _react2.default.createElement(
      'h1',
      null,
      'Allgemeine Gesch\xE4ftsbedingungen'
    ),
    _react2.default.createElement(
      'strong',
      null,
      'Allgemeine Gesch\xE4ftsbedingungen (AGB) der iReparatur.ch / remarket.ch GmbH, Kunden- und Verbraucherinformationen und Nutzungsbedingungen'
    ),
    _react2.default.createElement(
      'p',
      null,
      'Die folgenden Allgemeinen Gesch\xE4ftsbedingungen (AGB) sind sowohl f\xFCr den Ankauf als auch f\xFCr den Verkauf von Produkten und Dienstleistungen der iReparatur.ch / remarket.ch GmbH g\xFCltig. Die AGB beziehen sich ausschliesslich auf das Internetportal www.remarket.ch.'
    ),
    _react2.default.createElement(
      'p',
      null,
      'Dar\xFCber hinaus gehende Bedingungen (hierunter fallen insbesondere Bedingungen seitens des Endkunden) sind nur g\xFCltig bei Anerkennung unsererseits in schriftlicher Form (Brief, E-Mail). Durch die Verwendung des Internetportals www.remarket.ch erkennen Sie diese Allgemeinen Gesch\xE4ftsbedingungen (AGB) ausdr\xFCcklich und ohne Bedingungen an. Es ist Ihnen freigestellt, diese AGB auszudrucken oder sie lokal auf Ihrem Endger\xE4t zu speichern. Verbraucher im Sinne dieser AGB werden als nat\xFCrliche Personen verstanden, wobei ihnen im Rahmen dieser Gesch\xE4ftsbeziehung keine selbst\xE4ndige oder gewerbliche berufliche T\xE4tigkeit zugerechnet werden kann. Unternehmer im Sinne dieser Allgemeinen Gesch\xE4ftsbedingungen sind entweder juristische oder nat\xFCrliche Personen oder rechtsf\xE4hige Personengesellschaften, die im Rahmen dieser Gesch\xE4ftsbeziehung eine gewerbliche oder selbst\xE4ndige berufliche T\xE4tigkeit aus\xFCben. Ein Kunde im Sinne dieser Allgemeinen Gesch\xE4ftsbedingungen kann sowohl ein Verbraucher als auch ein Unternehmer sein.'
    ),
    _react2.default.createElement(
      'ul',
      null,
      _react2.default.createElement(
        'ol',
        null,
        _react2.default.createElement(
          'strong',
          null,
          '1. G\xFCltigkeit der AGB:'
        ),
        _react2.default.createElement(
          'p',
          null,
          'Vertragspartner, Privatkunden sowie Unternehmerkunden.'
        )
      ),
      _react2.default.createElement(
        'ol',
        null,
        _react2.default.createElement(
          'strong',
          null,
          '2. Vertragssprache und anwendbares Recht:'
        ),
        _react2.default.createElement(
          'p',
          null,
          'Die ausschliessliche Vertragssprache ist Deutsch. Abgesehen von vorrangig zwingenden Vorschriften des ausl\xE4ndischen Rechts wird allein das schweizer Recht angewendet, wobei die Vorschriften des UN-Kaufrechts ausgeschlossen werden.'
        )
      )
    ),
    _react2.default.createElement(
      'h2',
      null,
      '\xA71 Allgemeine Nutzungsbedingungen'
    ),
    _react2.default.createElement(
      'ul',
      null,
      _react2.default.createElement(
        'ol',
        null,
        '1. W\xE4hrend des Besuchs der Internetplattform remarket.ch werden bestimmte Informationen automatisch gespeichert. Diese werden nur f\xFCr interne Zwecke des Anbieters iReparatur.ch / remarket.ch GmbH genutzt und nicht an Dritte weitergegeben.'
      ),
      _react2.default.createElement(
        'ol',
        null,
        '2. Auch erhaltene und gesendete E-Mails werden von der iReparatur.ch / remarket.ch GmbH f\xFCr interne Zwecke gespeichert.'
      ),
      _react2.default.createElement(
        'ol',
        null,
        '3. Vertragstexte werden im Rahmen unserer kaufm\xE4nnischen Buchhaltung entsprechend der gesetzlichen Vorgaben gespeichert. Dennoch raten wir Ihnen, die vom Anbieter per E-Mail versendeten vertraglichen Unterlagen separat auszudrucken beziehungsweise abzuspeichern. Falls Sie Ihren Verkauf oder Ankauf direkt \xFCber www.remarket.ch durchgef\xFChrt haben, haben Sie die M\xF6glichkeit, Ihre Daten sowie auch den Status Ihrer aktuellen An- und Verk\xE4ufe zu jeder Zeit \xFCber "Mein Konto" unter www.remarket.ch mit dem entsprechenden Link aufrufen.'
      )
    ),
    _react2.default.createElement(
      'h2',
      null,
      '\xA72 Nutzung der Website'
    ),
    _react2.default.createElement(
      'ul',
      null,
      _react2.default.createElement(
        'ol',
        null,
        '1. Zur Nutzung unserer Dienste m\xFCssen Sie voll gesch\xE4ftsf\xE4hig sein und sich zuvor auf unseren Seiten registriert haben. Durch die Registrierung entstehen keine Kosten.'
      ),
      _react2.default.createElement(
        'ol',
        null,
        '2. F\xFCllen Sie das Anmeldeformular bei der Registrierung zur Er\xF6ffnung eines Kontos wahrheitsgem\xE4ss und vollst\xE4ndig aus. F\xFCr den Fall, dass sich Ihre Daten \xE4ndern, haben Sie selbst die Verantwortung f\xFCr die Aktualisierung. Jegliche \xC4nderungen werden online nach erfolgter Anmeldung unter dem Men\xFCpunkt "Mein Konto" vorgenommen. Zur Vermeidung von Missbrauch sind Sie in der Pflicht, die Registrierungsdaten geheimzuhalten und diese Dritten nicht mitzuteilen. Jeder Kunde ist berechtigt, nur ein Kundenkonto zu er\xF6ffnen.'
      ),
      _react2.default.createElement(
        'ol',
        null,
        '3. F\xFCr den Fall, dass Sie Ihre Daten auf unserem Portal l\xF6schen m\xF6chten, senden Sie unserem Kundendienst bitte eine Nachricht. Der Kundendienst ist unter der URL https://www.remarket.ch/kontakt erreichbar. Wenn Sie die L\xF6schung beantragen, erfolgt die Sperrung des Kontos unverz\xFCglich. Wenn alle noch offenen Verkaufs- und Ankaufsvorg\xE4nge abgeschlossen sind, erfolgt die L\xF6schung der hinterlegten Daten separat nach Ablauf der gesetzlichen Aufbewahrungsfristen. Wir bitten Sie daher, zuvor Kopien zur Sicherung s\xE4mtlicher Daten Ihres Kontos anzufertigen, wenn Sie diese noch ben\xF6tigen.'
      )
    ),
    _react2.default.createElement(
      'h2',
      null,
      '\xA73 Batteriegesetz'
    ),
    _react2.default.createElement(
      'p',
      null,
      'In unseren Lieferungen k\xF6nnen Batterien oder Akkus enthalten sein. Gem\xE4ss Batteriegesetz (BattG) haben wir daher die Pflicht, Sie auf die folgenden Bestimmungen hinzuweisen: Gebrauchte Akkus und Batterien m\xFCssen gem\xE4ss den gesetzlichen Bestimmungen ordnungsgem\xE4ss zur\xFCckgegeben werden und d\xFCrfen nicht im Hausm\xFCll entsorgt werden. Bei nicht sachgem\xE4sser Entsorgung oder Lagerung von Altbatterien k\xF6nnen die darin befindlichen Schadstoffe der Umwelt oder Ihrer Gesundheit schaden. Dar\xFCber hinaus enthalten Batterien Rohstoffe wie zum Beispiel Mangan, Eisen, Zink oder Nickel und k\xF6nnen recycelt werden. Daher haben Sie die M\xF6glichkeit, die Batterien nach Verwendung an uns zur\xFCckzuschicken oder sie beispielsweise im Handel, an Sammelstellen der Kommune oder in unserem Versandlager kostenlos zur\xFCckzugegeben. Dabei ist die Abgabe in den Verkaufsstellen auf die f\xFCr Endnutzer \xFCbliche Menge beschr\xE4nkt und zudem auf solche Altbatterien beschr\xE4nkt, die vom Anbieter als neue Batterien im Sortiment gef\xFChrt werden.'
    ),
    _react2.default.createElement(
      'p',
      null,
      'Das hier dargestellte Symbol der durchgekreuzten M\xFClltonne hat die Bedeutung, dass die Batterien und Akkus nicht im Hausm\xFCll entsorgt werden d\xFCrfen. Dar\xFCber hinaus finden Sie unter diesem Symbol zus\xE4tzlich die nachstehenden Bezeichnungen mit den folgenden Bedeutungen:'
    ),
    _react2.default.createElement(
      'ul',
      null,
      _react2.default.createElement(
        'li',
        null,
        'Pb \u2013 Die Batterie enth\xE4lt Blei'
      ),
      _react2.default.createElement(
        'li',
        null,
        'Cd \u2013 Die Batterie enth\xE4lt Cadmium'
      ),
      _react2.default.createElement(
        'li',
        null,
        'Hg \u2013 Die Batterie enth\xE4lt Quecksilber'
      )
    ),
    _react2.default.createElement(
      'h2',
      null,
      '\xA74 Ankauf gebrauchter Produkte durch den Anbieter (Ank\xE4ufe)'
    ),
    _react2.default.createElement(
      'p',
      null,
      'Inhalt: Einsendung im Rahmen des Ankaufs; Kosten der Einsendung; Datensicherung; Transportrisiko'
    ),
    _react2.default.createElement(
      'ul',
      null,
      _react2.default.createElement(
        'li',
        null,
        'a. Die Einsendung angebotener Ger\xE4te ist f\xFCr den Kunden kostenlos. Diese Regelung ist jedoch nur g\xFCltig in dem Fall, in dem der Kunde den per E-Mail oder Download zur Verf\xFCgung gestellten Versandaufkleber der iReparatur.ch / remarket.ch GmbH f\xFCr Die Post (Schweiz/Liechtenstein) verwendet. Mithilfe des Versandaufklebers kann das fachgerecht verpackte Ger\xE4t bei jeder Postagentur,  Postfiliale oder jeder Packstation in dr Schweiz kostenlos versendet werden. Wird der Versandaufkleber nicht genutzt, so hat der Kunde die Kosten f\xFCr die \xDCbersendung zu tragen.'
      ),
      _react2.default.createElement(
        'li',
        null,
        'b. Andere Versandmethoden werden nur nach Absprache und bei gegenseitigem Einverst\xE4ndnis \xFCbernommen. Der Kunde ist selbst daf\xFCr verantwortlich, dass er bei einer Postlieferung die per E-Mail erhaltene Etikette der Schweizerischen Post auf das Paket klebt.'
      ),
      _react2.default.createElement(
        'li',
        null,
        'c. Der Kunde tr\xE4gt das Risiko eines Verlustes oder einer Besch\xE4digung, die auf dem Transportweg bis zu der Aush\xE4ndigung der Sendung an die iReparatur.ch / remarket.ch GmbH durch das Transportunternehmen entsteht. Aus diesem Grund ist f\xFCr den Versand eine geeignete Verpackung zu w\xE4hlen.'
      ),
      _react2.default.createElement(
        'li',
        null,
        'd. Etwaige allf\xE4llige Sch\xE4den durch den Transport werden erst dann \xFCbernommen, wenn die Sendung an das Transportunternehmen \xFCbergeben wurde.'
      ),
      _react2.default.createElement(
        'li',
        null,
        'e. Allf\xE4llige Transportsch\xE4den werden erst ab der Aufgabe an das f\xFCr den Transport verantwortliche Unternehmen \xFCbernommen.'
      ),
      _react2.default.createElement(
        'li',
        null,
        'f. Handelt es sich um gr\xF6ssere Ger\xE4te wie zum Beispiel ein iMac oder ein Display, die sich nicht in einer Originalverpackung befinden (das heisst inklusive des originalen Verpackungsmaterials innen), so tr\xE4gt der Verk\xE4ufer daf\xFCr die Verantwortung, dass das Ger\xE4t f\xFCr die Versendung mit der Schweizerischen Post ordnungsgem\xE4ss verpackt ist. Diese Regelung ist auch bei Paketen g\xFCltig, in denen mehrere Produkte enthalten sind.'
      ),
      _react2.default.createElement(
        'li',
        null,
        'g. Wenn in den hier genannten F\xE4llen ein Schaden w\xE4hrend des Transports auftritt und das Ger\xE4t nicht sachgem\xE4ss verpackt wurde, so wird der Geldwert des Schadens vom Ankaufspreis abgezogen.'
      ),
      _react2.default.createElement(
        'li',
        null,
        'h. F\xFCr den korrekten Versand per Schweizerischer Post existiert eine entsprechende Anleitung, die unter der Domain http://www.post.ch/post-startseite/post-geschaeftskunden/post-logistik/post-distribution-national/post-pakete/post-zusatzangebote-pakete/post-verpackung/post-paketversand.pdf abgerufen werden kann.'
      ),
      _react2.default.createElement(
        'li',
        null,
        'i. Nach der Angebotsabgabe hat das vom Kunden angebotene Ger\xE4t innerhalb einer Frist von 7 Tagen bei der iReparatur.ch / remarket.ch GmbH einzugehen.'
      ),
      _react2.default.createElement(
        'li',
        null,
        'j. Wenn der Kunde an die iReparatur.ch / remarket.ch GmbH unfreie Sendungen schickt, so kann der Anbieter die Annahme ablehnen; in diesem Fall tr\xE4gt der Kunde die Kosten.'
      )
    ),
    _react2.default.createElement(
      'h2',
      null,
      '\xA75 Ankauf'
    ),
    _react2.default.createElement(
      'ul',
      null,
      _react2.default.createElement(
        'ol',
        null,
        '1. Die iReparatur.ch / remarket.ch GmbH l\xF6scht bei einem Ankauf s\xE4mtliche auf den gebrauchten Ger\xE4ten enthaltenen Informationen. F\xFCr die Sicherung aller pers\xF6nlichen und wichtigen Daten hat der Verk\xE4ufer aus diesem Grund selbst die Verantwortung.'
      ),
      _react2.default.createElement(
        'ol',
        null,
        '2. Die errechneten Preise (Verkaufsformular der Internetseite, vor Ort im Gesch\xE4ft, per Telefon oder per E-Mail) sind unverbindlich. Bei dem Vorgang (und bei allen anderen Ank\xE4ufen per Telefon, vor Ort oder per E-Mail) handelt es sich um eine Offerte seitens des Verk\xE4ufers an die iReparatur.ch / remarket.ch GmbH.'
      ),
      _react2.default.createElement(
        'ol',
        null,
        '3. Die Berechnung des endg\xFCltigen Preises erfolgt erst nach der gr\xFCndlichen \xDCberpr\xFCfung des eingesendeten Ger\xE4tes.'
      ),
      _react2.default.createElement(
        'ol',
        null,
        '4. Durch das Akzeptieren dieser AGB und durch den anschliessenden Abschluss des Ankauf-Vorganges best\xE4tigt der Verk\xE4ufer, dass das offerierte Ger\xE4t vollumf\xE4nglich Eigentum des Verk\xE4ufers ist.'
      ),
      _react2.default.createElement(
        'ol',
        null,
        '5. L\xE4nderabh\xE4ngige Regelungen:'
      ),
      _react2.default.createElement(
        'ol',
        null,
        '5.1. Schweiz: Bei dem Verk\xE4ufer handelt es sich um eine nat\xFCrliche oder eine juristische Person, die ihren Wohn- oder Firmensitz in der Schweiz oder Liechtenstein hat.'
      ),
      _react2.default.createElement(
        'ol',
        null,
        '5.2. Deutschland: Bei dem Verk\xE4ufer handelt es sich um eine nat\xFCrliche oder eine juristische Person, die ihren Wohn- oder Firmensitz in Deutschland hat (oder EU).'
      )
    ),
    _react2.default.createElement(
      'ul',
      null,
      _react2.default.createElement(
        'ol',
        null,
        '6. Wenn Ger\xE4te als vollumf\xE4nglich oder teilweise defekt anzusehen sind, so ist in der Regel eine detailliertere Pr\xFCfung n\xF6tig. Durch die Abgabe beziehungsweise den Versand des Ger\xE4tes an Remarket autorisiert der Verk\xE4ufer den Anbieter Remarket daher, das Ger\xE4t (wenn n\xF6tig) durch technische \xC4nderungen in einen Zustand zu bringen, der die Durchf\xFChrung von Tests erlaubt. Dabei ist Remarket nicht f\xFCr Sch\xE4den verantwortlich, die sich im Rahmen dieser Tests weiterentwickeln (sofern sie schon vorhanden sind) oder neu entstehen.'
      ),
      _react2.default.createElement(
        'ol',
        null,
        '7. Zustand des Ger\xE4ts (Vertragsgegenstand)'
      ),
      _react2.default.createElement(
        'ol',
        null,
        '7.1. Der Kunde gibt bei der Angebotsabgabe eine Beschreibung des visuellen Zustands, der Funktionstauglichkeit sowie der Art des Ger\xE4ts ab. Diese Beschreibung wird im Falle des Vertragsschlusses als ausdr\xFCckliche Zustandsbeschreibung ein Bestandteil des Kaufvertrages.'
      ),
      _react2.default.createElement(
        'ol',
        null,
        '7.2. Aus diesem Grund ist der Kunde zur richtigen, vollst\xE4ndigen und wahrheitsgem\xE4ssen Wiedergabe des angebotenen Ger\xE4tes verpflichtet. Der Anbieter stellt dem Kunden f\xFCr die ordnungsgem\xE4sse Beschreibung einen Katalog mit Kriterien zur Verf\xFCgung, die auszuf\xFCllen ist, um das Ger\xE4t zu beschreiben. Erfolgt diese Beschreibung nicht, kann die iReparatur.ch / remarket.ch GmbH das angebotene Ger\xE4t nicht ordnungsgem\xE4ss bewerten. Der Anbieter weist ausdr\xFCcklich darauf hin, dass diese Beschreibung des Kunden das massgebliche Kriterium f\xFCr die Berechnung des Preises darstellt.'
      ),
      _react2.default.createElement(
        'ol',
        null,
        '7.3. Die folgenden Ger\xE4te sind ausgeschlossen und werden von Remarket nicht angenommen:'
      )
    ),
    _react2.default.createElement(
      'ul',
      null,
      _react2.default.createElement(
        'li',
        null,
        '- Demo-Ger\xE4te'
      ),
      _react2.default.createElement(
        'li',
        null,
        '- Modelle, die vom Hersteller nicht f\xFCr den Verkauf vorgesehen sind'
      ),
      _react2.default.createElement(
        'li',
        null,
        '- Ger\xE4te, die nicht \xFCber eine IMEI-Nummer verf\xFCgen (Beispiel: IMEI Aufkleber im Batteriefach fehlt)'
      ),
      _react2.default.createElement(
        'li',
        null,
        '- Ger\xE4te, die nicht zug\xE4nglich sind, beispielsweise aufgrund einer PIN-Sperre / Cloud-Sperre / Diebstahlsperre'
      ),
      _react2.default.createElement(
        'li',
        null,
        '- F\xE4lschungen'
      ),
      _react2.default.createElement(
        'li',
        null,
        '- Ge\xF6ffnete bzw. modifizierte Ger\xE4te'
      ),
      _react2.default.createElement(
        'li',
        null,
        '- Ger\xE4te mit nicht originalen Ersatzteilen'
      )
    ),
    _react2.default.createElement(
      'p',
      null,
      'Remarket weist ausdr\xFCcklich darauf hin, dass die R\xFCcksendung in einem solchen Fall auf Kosten des einsendenden Kunden erfolgt.'
    ),
    _react2.default.createElement(
      'ol',
      null,
      '7.4. Neuger\xE4te: Unter der Definition neu verstehen wir ein komplett neues Ger\xE4t, welches sich noch in einer verschweissten Originalverpackung befindet inkl. Garantieschein. Falls kein g\xFCltiger Garantieschein vorhanden ist, werden 40.00 CHF vom Verkaufspreis abgezogen.'
    ),
    _react2.default.createElement(
      'h2',
      null,
      '\xA76 Sicherung von Daten'
    ),
    _react2.default.createElement(
      'ul',
      null,
      _react2.default.createElement(
        'ol',
        null,
        '1. Der Kunde ist f\xFCr die Sicherung von Daten wie beispielsweise Adressbucheintr\xE4gen, Fotos, Musik oder Nachrichten vor der Einsendung verantwortlich. Remarket f\xFChrt i.d.R. w\xE4hren der Warenannahme und Pr\xFCfung des Ger\xE4ts Datenl\xF6schungen und R\xFCcksetzungen in den Auslieferungszustand sowie unter Umst\xE4nden Updates der Software durch. Diese f\xFChren in der Regel dazu, dass vorhandene Daten dauerhafte gel\xF6scht werden. Remarket ist nicht f\xFCr die Sicherung und den Erhalt von pers\xF6nlichen Daten verantwortlich. Remarket ist dar\xFCber hinaus nicht daf\xFCr verantwortlich, dass Daten nicht in die H\xE4nde von Dritten gelangen. Remarket weist ausdr\xFCcklich darauf hin, dass die iReparatur.ch / remarket.ch GmbH zur L\xF6schung der Daten nicht verpflichtet ist und nicht sichergestellt werden kann, dass alle Daten beziehungsweise Fragmente vollst\xE4ndig und unwiderruflich gel\xF6scht werden.'
      ),
      _react2.default.createElement(
        'ol',
        null,
        '2. Bevor Datentr\xE4ger bzw. Ger\xE4te mit internen Datentr\xE4gern (hierunter fallen beispielsweise interne sowie externe Festplatten, andere Massespeicher, Speicherkarten sowie USB-Sticks) zur\xFCckgesendet werden, hat der Kunde selbst f\xFCr die Sicherung und L\xF6schung der darauf gespeicherten Daten zu sorgen. Remarket steht nicht f\xFCr die Sicherung der Daten und deren L\xF6schung vor einem Weiterverkauf des Ger\xE4ts ein.'
      ),
      _react2.default.createElement(
        'ol',
        null,
        '3. Dar\xFCber hinaus k\xF6nnen nach dem Versand des angebotenen Ger\xE4ts durch den Kunden beigef\xFCgte Speichermedien (wenn zutreffend) nicht zur\xFCckgegeben oder noch gespeicherte Daten herausgegeben werden.'
      ),
      _react2.default.createElement(
        'ol',
        null,
        '4. Die iReparatur.ch / remarket.ch GmbH wird vom Kunden von allen Anspr\xFCchen freigestellt, die daraus resultieren k\xF6nnen, dass auf dem Ger\xE4t beziehungsweise den eingesendeten Datentr\xE4gern, \xFCber die der Kaufvertrag geschlossen wurde, noch Daten vorhanden waren. Diese Regelung hat insbesondere auch f\xFCr etwaige Anspr\xFCche Dritter G\xFCltigkeit.'
      ),
      _react2.default.createElement(
        'ol',
        null,
        '5. Der Kunde sendet das angebotene Ger\xE4t mit s\xE4mtlichen Speichermedien ein, die f\xFCr einen ordnungsgem\xE4ssen Betrieb notwendig oder im Lieferumfang des Herstellers enthalten sind.'
      )
    ),
    _react2.default.createElement(
      'h2',
      null,
      '\xA77 Gegenofferte'
    ),
    _react2.default.createElement(
      'ul',
      null,
      _react2.default.createElement(
        'ol',
        null,
        '1. Wenn sich im Rahmen der \xDCberpr\xFCfung des Ger\xE4ts herausstellt, dass der tats\xE4chliche Zustand von dem Zustand abweicht, der in der Zustandsbeschreibung des Kunden angegeben war, wird das Angebot abgelehnt. Remarket sendet dem Kunden per E-Mail ein neues Angebot zum Ankauf, das ein nach den Kriterien von Remarket und dem Zustand der Ware angepassten Preis enth\xE4lt. Dieses Angebot wird als Gegenangebot bzw. Gegenofferte bezeichnet.'
      ),
      _react2.default.createElement(
        'ol',
        null,
        '1.1. Der Kunde hat die M\xF6glichkeit, das Gegenangebot innerhalb von 14 Tagen nach Eingang der E-Mail anzunehmen oder abzulehnen. Dazu wird dem Kunden per Link in der E-Mail eine Webseite zur Verf\xFCgung gestellt. Wenn der Kunde das  Gegenangebot von Remarket annimmt, so kommt auf Basis des Gegenangebots ein Vertrag zustande. Wenn der Kunde auf das via E-Mail gesendete Gegenangebot der iReparatur.ch / remarket.ch GmbH in einem Zeitraum von 14 Tagen weder mit Annahme noch mit Ablehnung reagiert, so gilt das Angebot als angenommen und der Kaufvertrag auf Basis des Gegenangebots kommt zustande. Der Kunde wird auf diese Folge seiner nicht erfolgten Reaktion im Rahmen des Gegenangebots ausdr\xFCcklich erneut aufmerksam gemacht.'
      ),
      _react2.default.createElement(
        'ol',
        null,
        '1.2. In dem Fall, dass der Kunde das Gegenangebot ablehnt, kommt der Vertrag nicht zustande. Remarket sendet dem Kunden das Produkt versandkostenfrei und auf eigenes Risiko an die angegebene Adresse. Diese R\xFCcksendung wird vorher per Mail angek\xFCndigt. Wenn das Paket aufgrund der Angabe einer falschen Adresse nicht zugestellt werden kann, fordert Remarket den Kunden per E-Mail zur Angabe einer korrekten Adresse innerhalb von f\xFCnf Tagen auf. Wenn der Kunde der ersten Aufforderung nicht nachkommt und auch einer weiteren Aufforderung innerhalb eines Zeitraums von drei Wochen nicht nachkommt, so erkl\xE4rt er, dass die betreffenden Produkte aus dem System entfernt werden sollen und das Eigentum an der Sache aufgegeben wird. Der Anspruch auf eine R\xFCcksendung der Produkte verf\xE4llt damit.'
      ),
      _react2.default.createElement(
        'ol',
        null,
        '2.  Wenn die entsprechend des von der iReparatur.ch / remarket.ch GmbH zur Verf\xFCgung gestellten Katalogs mit Kriterien durchgef\xFChrte Beschreibung des Zustands des angebotenen Ger\xE4tes vom tats\xE4chlichen Zustand, der von der iReparatur.ch / remarket.ch GmbH w\xE4hrend der eigenen Pr\xFCfung festgestellt wird, erheblich abweicht, so hat die iReparatur.ch / remarket.ch GmbH das Recht, das Kaufangebot abzulehnen. In diesem Fall kommt ein Vertrag zwischen der iReparatur.ch / remarket.ch GmbH und dem Kunden \xFCber den Verkauf des Ger\xE4ts nicht zustande.'
      ),
      _react2.default.createElement(
        'ol',
        null,
        '3. Wenn die erste Kalkulation im Rahmen einer kurzen Kontrolle auf einer der Verkaufsfl\xE4chen oder Ankaufsfl\xE4chen zu einer anderen Offerte als die nachfolgende umfangreiche Bewertung f\xFChrt, etwa weil bei weiterf\xFChrenden \xDCberpr\xFCfungen M\xE4ngel ersichtlich werden, wird der das Ger\xE4t anbietende Kunde kontaktiert.'
      ),
      _react2.default.createElement(
        'ol',
        null,
        '4. Bei Ausl\xF6sung der Zahlung oder die telefonische beziehungsweise elektronische (per E-Mail) Zustimmung seitens der iReparatur.ch / remarket.ch GmbH an den Kunden oder die telefonische beziehungsweise elektronische (per E-Mail) Zustimmung seitens des Kunden an die iReparatur.ch / remarket.ch GmbH wechselt das angebotene Produkt endg\xFCltig in den Besitz der iReparatur.ch / remarket.ch GmbH.'
      )
    ),
    _react2.default.createElement(
      'h2',
      null,
      '\xA78  Auszahlungen bei einem Ankauf'
    ),
    _react2.default.createElement(
      'ul',
      null,
      _react2.default.createElement(
        'ol',
        null,
        '1. Bei der Unterbreitung eines Angebot zum Ankauf von gebrauchten Ger\xE4ten kann sich der Kunde f\xFCr die Auszahlung des vereinbarten Preises zwischen der Auszahlung auf ein Bankkonto und dem Kauf von Guthaben bei Remarket entscheiden.'
      ),
      _react2.default.createElement(
        'ol',
        null,
        '2. Wenn sich der Kunde f\xFCr die Auszahlung auf ein Bankkonto entscheidet, so erfolgt die Auszahlung unmittelbar nach erfolgter Annahme des entsprechenden Angebotes beziehungsweise Gegenangebots auf das vom Kunden angegebene Bankkonto. In Abh\xE4ngigkeit des jeweiligen Landes und der Bank kann die Auszahlung auf  dem Bankkonto zwei bis sieben Werktage in Anspruch nehmen. Wenn bei einer \xDCberweisung ins Ausland zus\xE4tzliche Kosten anfallen, so sind diese vom Kunden zu tragen. Es wird empfohlen, das Bankinstitut nach etwaigen Zusatzkosten zu fragen. Wenn sich der Kunde f\xFCr eine  Auszahlung in Form von Remarket-Guthaben (Credits) entscheidet, dann gelten die daf\xFCr vorgesehenen Nutzungsbedingungen. (siehe \xA711 Regelungen zu Credits)'
      )
    ),
    _react2.default.createElement(
      'h2',
      null,
      '\xA79 Eigentum und Zusicherungen hinsichtlich der Ger\xE4te im Rahmen des Ankaufs / Freistellung durch den Kunden'
    ),
    _react2.default.createElement(
      'ul',
      null,
      _react2.default.createElement(
        'ol',
        null,
        '1. Bei Abgabe eines Angebots an Remarket sichert der Kunde zu, dass er der rechtm\xE4ssige Eigent\xFCmer des angebotenen Ger\xE4tes ist beziehungsweise \xFCber eine Legitimation zum Verkauf des Ger\xE4tes verf\xFCgt. Dar\xFCber hinaus sichert der Kunde zu, dass keine Rechten Dritter bei den angebotenen Ger\xE4ten ber\xFChrt werden.'
      )
    ),
    _react2.default.createElement(
      'h2',
      null,
      '\xA710 Zahlungsmethoden'
    ),
    _react2.default.createElement(
      'p',
      null,
      _react2.default.createElement(
        'strong',
        null,
        'Schweiz:'
      )
    ),
    _react2.default.createElement(
      'p',
      null,
      'Alle Zahlungen werden ausschliesslich per \xDCberweisung auf Bankkonten in der Schweiz und Liechtenstein, Paypal oder per Guthaben (Credits) des Verwenders get\xE4tigt. Davon abweichende Modalit\xE4ten (hierzu geh\xF6ren insbesondere Bar-Auszahlungen) sind ausschliesslich in unserer Filiale m\xF6glich.'
    ),
    _react2.default.createElement(
      'h3',
      null,
      'Versandkosten'
    ),
    _react2.default.createElement(
      'p',
      null,
      'F\xFCr die Standard Lieferung innerhalb der Schweiz und des F\xFCrstentums Liechtenstein ist die Bestellung kostenlos. F\xFCr eine Express Mond Sendung berechnen wir 18.00 CHF. Der Anbieter teilt dem Kunden die Kosten f\xFCr den Versand auf den Produktseiten sowie im Warenkorbsystem und auf der Bestellseite noch einmal deutlich mit.'
    ),
    _react2.default.createElement(
      'h3',
      null,
      'Vorauskasse'
    ),
    _react2.default.createElement(
      'p',
      null,
      'Wenn die Zahlungsart Vorauskasse ausgew\xE4hlt wird, teilt Remarket dem Kunden die entsprechende Bankverbindung im Rahmen der Auftragsbest\xE4tigung mit und liefert die Ware im Anschluss an den Eingang der Zahlung. Wenn die Bestellung eingeht, so reserviert der Verk\xE4ufer die Ware f\xFCr den Kunden (2 Tage). Wenn die Zahlungsart Vorauskasse vereinbart wurde und die vollst\xE4ndige Zahlung nicht im Zeitraum von 2 Tagen ab Abschluss des Vertrages erfolgt ist, so hat der Verk\xE4ufer das Recht, anderweitig \xFCber die Ware zu verf\xFCgen. Unbezahlte Bestellungen werden nach 3 Werktagen storniert.'
    ),
    _react2.default.createElement(
      'h3',
      null,
      'Bob Invoice - Zahlung per Rechnung'
    ),
    _react2.default.createElement(
      'p',
      null,
      'Valora Schweiz AG ist der Dienstleister, der Ihnen erlaubt, Ihre Bestellung auf Rechnung bzw. bis zu drei Monatsraten einfach und sicher zu bezahlen. Der minimale Bestellwert ist 50.00 CHF und der maximale Bestellungswert f\xFCr Ihre Bestellung via Bob Invoice betr\xE4gt f\xFCr bestehende Kunden und Neukunden CHF 1000.00. Die Zahlungsfrist betr\xE4gt 30 Tage. Aus Sicherheitsgr\xFCnden m\xFCssen die Rechnungsadresse und die Lieferungsadresse \xFCbereinstimmen. Es ist nur m\xF6glich als Privatkunde eine Bestellung auf Rechnung zu t\xE4tigen. Bevor die Transaktion bewilligt wird, behaltet sich Valora Schweiz AG das Recht vor, eine Bonit\xE4tspr\xFCfung bei der Firma CRIF AG in Z\xFCrich (Tel.: 044 913 50 50, ',
      _react2.default.createElement(
        'a',
        { href: 'https://www.crif.ch', target: '_blank' },
        'www.crif.ch'
      ),
      ') durchzuf\xFChren.'
    ),
    _react2.default.createElement(
      'p',
      null,
      'Bei erfolgreicher Bonit\xE4tspr\xFCfung kann remarket.ch eine Adressbest\xE4tigung und Vorder- sowie R\xFCckseite der Identit\xE4tskarte bzw. Pass verlangen, um die Bestellung zu pr\xFCfen und definitiv zu gew\xE4hren.'
    ),
    _react2.default.createElement(
      'p',
      null,
      '\xABBei Kauf auf Rechnung bzw. Ratenzahlung via Bob Invoice gelten zus\xE4tzlich die ',
      _react2.default.createElement(
        'a',
        { href: 'https://bob.ch/de/firmenkunden/service/b2b-agb/bob-invoice/', target: '_blank' },
        'AGB der Valora Schweiz AG.'
      ),
      '\xBB'
    ),
    _react2.default.createElement(
      'p',
      null,
      'Falls eine Bestellung via Bob Invoice storniert wird, fallen eine Bearbeitungsgeb\xFChr von 20.00 CHF an.'
    ),
    _react2.default.createElement(
      'h3',
      null,
      'Paypal'
    ),
    _react2.default.createElement(
      'p',
      null,
      'Wenn der Kunde  den Rechnungsbetrag \xFCber den Anbieter PayPal begleichen m\xF6chte, muss er dort registriert sein oder zun\xE4chst ein Konto erstellen und sich registrieren. Im Anschluss an die Legitimation mit Zugangsdaten kann der Kunde die Zahlungsanweisung an den Anbieter best\xE4tigen.'
    ),
    _react2.default.createElement(
      'h3',
      null,
      'Sofort\xFCberweisung'
    ),
    _react2.default.createElement(
      'p',
      null,
      'Wenn der Kunde den Rechnungsbetrag \xFCber den Anbieter Sofort\xFCberweisung begleichen m\xF6chte, muss er \xFCber ein f\xFCr das Online-Banking freigeschaltetes Bankkonto in der Schweiz verf\xFCgen. Nach erfolgter Legitimation mit Zugangsdaten kann der Kunde die Zahlung an Remarket anweisen und best\xE4tigen.'
    ),
    _react2.default.createElement(
      'h3',
      null,
      'Kreditkarte'
    ),
    _react2.default.createElement(
      'p',
      null,
      'Wenn der Kunde per Kreditkarte bezahlen m\xF6chte, wird er nach Abschicken der Bestellung zum Anbieter Datatans weitergeleitet. Dort erfolgt die Buchung beim Klicken des Auswahlfeldes "Zahlung".'
    ),
    _react2.default.createElement(
      'p',
      null,
      'W\xE4hrend des Bestellvorgangs werden jeweils weitere Hinweise zu den Zahlungsmodalit\xE4ten angezeigt.'
    ),
    _react2.default.createElement(
      'ul',
      null,
      _react2.default.createElement(
        'li',
        null,
        '1. Falls sich der Kunde f\xFCr eine der Zahlungsweisen PayPal, Kreditkarte, Vorauskasse oder Sofort\xFCberweisung entscheidet, so f\xFChrt der Anbieter unmittelbar nach dem vollst\xE4ndigem Zahlungseingang die Bestellung aus. Im Anschluss sendet er die vom Kunden  bestellten Produkte an die vom Kunden angegebene Adresse. Der Anbieter informiert den Kunden \xFCber diesen Versandvorgang, indem er eine Versandbest\xE4tigung per Mail sendet. Dar\xFCber hinaus befindet sich in dieser E-Mail ein Link zur Rechnung des Kunden.'
      ),
      _react2.default.createElement(
        'li',
        null,
        '2. Die vom Kunden bestellten Produkte bleiben so lange im Eigentum des Anbieters, bis der Rechnungsbetrag vollst\xE4ndig beglichen wurde. Diese Regelung gilt \u2013 nur  gegen\xFCber Unternehmern \u2013 auch bis zum vollst\xE4ndigen Begleichen k\xFCnftiger Forderungen, die aus laufenden Gesch\xE4ftsverbindungen zzgl. Zinsen und Kosten stammen.'
      ),
      _react2.default.createElement(
        'li',
        null,
        '3. Wenn sich der Kunde mit Zahlungsverpflichtungen dem Anbieter Remarket gegen\xFCber in Verzug befindet, werden alle bestehenden Forderungen mit unmittelbarer Wirkung f\xE4llig.'
      ),
      _react2.default.createElement(
        'li',
        null,
        '4. Grunds\xE4tzlich kann der Kunde die Zahlung sowohl per Remarket-Guthaben (Credits) als auch via Vorauskasse (Bank\xFCberweisung), Sofort\xFCberweisung, Kreditkarte oder \xFCber den Dienst PayPal vornehmen.'
      ),
      _react2.default.createElement(
        'li',
        null,
        'a. Wenn der Kunde die Zahlungsart Vorauskasse ausw\xE4hlt, teilt der Anbieter dem Kunden im Anschluss in der Bestellbest\xE4tigung seine Bankverbindung mit. Diese wird zus\xE4tzlich auch auf der Abschlussseite angegeben, wenn der Auftrag seitens des Kunden aufgegeben wurde. Wenn das vorhandene Guthaben beim Anbieter iReparatur.ch / remarket.ch GmbH nicht vollst\xE4ndig zur Bezahlung des entsprechenden Auftrages ausreicht, so wird das Guthaben mit dem f\xE4lligen Betrag verrechnet beziehungsweise auf diesen angerechnet. Die Zahlungsfrist (entscheidend ist der Eingang auf dem Konto des Anbieters) betr\xE4gt in diesem Fall 7 Tage. Wenn per Kreditkarte gezahlt wird, wird der f\xE4llige Betrag erhoben, sobald der Vertrag abgeschlossen wurde.'
      ),
      _react2.default.createElement(
        'li',
        null,
        'b. Die Payment Network AG (Betreiber des Online-Dienstes "Sofort\xFCberweisung"),  hat eine Versicherung zu Gunsten des Kunden abgeschlossen. Diese Versicherung ersetzt Sch\xE4den bei Missbrauch nach bestimmten Massgaben, wobei die Massgaben bei dem Unternehmen selbst eingesehen werden k\xF6nnen. Durch die Versicherung soll der Kunde im Rahmen des Umfangs der Versicherung vor etwaigen Risiken der Haftung gesch\xFCtzt werden. Remarket weist in diesem Zusammenhang darauf hin, dass viele Sparkassen und Banken davon ausgehen, dass eine Verwendung des Online-Dienstes "Sofort\xFCberweisung" wegen der Nutzung von PIN und TAN zu einer Verlagerung der Haftung bei etwaigen Missbr\xE4uchen durch Dritte f\xFChrt. Diese Auffassung kann dazu f\xFChren, dass sich die Bank beim Auftreten eines Missbrauchs weigert, einen etwaigen Schaden zu \xFCbernehmen. In diesem Fall tr\xE4gt der Kunde den Schaden.'
      ),
      _react2.default.createElement(
        'li',
        null,
        'c. Wenn der Kunde die Zahlungsart PayPal ausw\xE4hlt, so erfolgt die Zahlung des Rechnungsbetrages mithilfe des Zahlungsanbieters PayPal. Der Kunde wird im Rahmen des Zahlungsprozesses auf die Internetseite www.paypal.ch des Anbieters Paypal geleitet. Um sich mit Zugangsdaten bei PayPal zu legitimieren und im Anschluss die Zahlungsanweisung an iReparatur.ch / remarket.ch GmbH zu best\xE4tigen, muss der Kunde bei PayPal registriert sein oder sich unter Umst\xE4nden neu registrieren. F\xFCr die Zahlungsvereinbarung zwischen dem Kunden und PayPal gelten die \u201EPayPal-Nutzungsbedingungen\u201C der PayPal (Europe) S.\xE0 r.l. & Cie, S.C.A. in ihrer jeweils g\xFCltigen Fassung. Diese k\xF6nnen auf der Internetseite www.paypal.com abgerufen werden.'
      ),
      _react2.default.createElement(
        'li',
        null,
        '5. Der Anbieter Remarket beh\xE4lt sich vor, dem Kunden nach eigenem Ermessen Wertgutscheine zur Verf\xFCgung zu stellen. Diese k\xF6nnen unter Umst\xE4nden an Bedingungen wie Mindestbestellwerte oder Fristen zur Einl\xF6sung gekn\xFCpft sein. Die geltenden Bedingungen sind dabei jeweils auf dem Gutschein vermerkt. Der Kunde kann pro Bestellung jeweils nur einen Gutschein einl\xF6sen. Bei Gutscheinen, welche an die Eigenschaft als Neukunde gekn\xFCpft sind, ist die Menge beschr\xE4nkt auf einen Gutschein pro Kunde. Der Anbieter Remarket beh\xE4lt sich vor, den Gutscheinwert unberechtigt eingel\xF6ster Gutscheine im Fall einer mengenm\xE4ssigen \xDCberschreitung aufzurechnen und/oder den Wert etwaigen Remarket-Guthabens auf www.remarket.ch zu verrechnen und/oder dem Kunden den entsprechenden Betrag in Rechnung zu stellen.'
      ),
      _react2.default.createElement(
        'li',
        null,
        '6. Wenn eine Sammelbestellung vorliegt, so verrechnet Remarket den Gutschein anteilig auf den Kaufpreis der bestellten Waren, sodass bei einem Widerruf (s. \xA7 3 V. dieser AGB) einzelner Waren der Sammelbestellung (als Teilwiderruf bezeichnet) der Kaufpreis reduziert wird, um den anteilig angerechneten Gutscheinwert auszuzahlen.'
      ),
      _react2.default.createElement(
        'li',
        null,
        '7. Wenn der Kunde diesen Vertrag widerruft, hat der Anbieter Remarket dem Kunden alle von diesem erhaltenen Zahlungen inklusive der Lieferkosten (exklusive der zus\xE4tzlichen Kosten, die sich aus der Wahl einer anderen Lieferart als der von Remarket angebotenen, g\xFCnstigsten Standardlieferung ergeben haben), sofort und sp\xE4testens innerhalb von vierzehn Tagen ab dem Tag des Eingangs der Mitteilung \xFCber den Widerruf dieses Vertrags zur\xFCckzuzahlen. Um die R\xFCckzahlung zu t\xE4tigen, verwendet Remarket dasselbe Zahlungsmittel, das der Kunde bei der urspr\xFCnglichen Zahlung eingesetzt hat. Diese Regelung gilt nicht, wenn der Kunde mit dem Anbieter  ausdr\xFCcklich etwas anderes vereinbart hat. In keinem Fall berechnet der Anbieter dem Kunden aufgrund einer R\xFCckzahlung Entgelte. Der Anbieter kann die R\xFCckzahlung so lange verweigern, bis er die Waren zur\xFCckerhalten hat beziehungsweise bis der Kunde den Nachweis \xFCber das Zur\xFCcksenden der Waren erbracht hat. Dabei gilt jeweils der fr\xFChere Zeitpunkt.'
      ),
      _react2.default.createElement(
        'li',
        null,
        '8. Der Kunde muss f\xFCr einen m\xF6glichen Wertverlust der Produkte nur in dem Fall aufkommen, dass dieser Wertverlust auf einen Umgang zur\xFCckzuf\xFChren ist, der zur Pr\xFCfung der Beschaffenheit, der Eigenschaften oder der Funktionsweise der Waren nicht notwendig ist.'
      ),
      _react2.default.createElement(
        'li',
        null,
        '9. Wenn  Ware zur\xFCckgesendet wird, ist daf\xFCr wenn m\xF6glich bitte die Originalverpackung sowie gegebenenfalls eine zus\xE4tzlich sch\xFCtzende Umverpackung zu verwenden. Wenn der Kunde nicht mehr im Besitz der originalen Verpackung ist, so kann er mit einer geeigneten alternativen Verpackung daf\xFCr sorgen, dass die Ware vor Transportsch\xE4den gesch\xFCtzt wird, um etwaige Anspr\xFCche wegen Schadenersatzes als Folge einer Besch\xE4digung der Ware durch mangelhafte Verpackung zu vermeiden. Bei der R\xFCcksendung ist darauf zu achten, dass die Ware inklusive der Original-Zubeh\xF6rteile retourniert wird. Wenn diese Zubeh\xF6rteile fehlen, so wird der Wert der R\xFCckzahlung um den entsprechenden Wert der Zubeh\xF6rteile reduziert. Die gleiche Regelung kommt zur Anwendung, wenn kein Original-Zubeh\xF6r zur\xFCckgegeben wird.'
      ),
      _react2.default.createElement(
        'li',
        null,
        '10. In Abh\xE4ngigkeit des jeweiligen Landes und der Bank kann die Auszahlung auf  dem Bankkonto zwei bis sieben Werktage in Anspruch nehmen. Wenn bei einer \xDCberweisung ins Ausland zus\xE4tzliche Kosten anfallen, so sind diese vom Kunden zu tragen. Es wird empfohlen, das Bankinstitut nach etwaigen Zusatzkosten zu fragen. Ferner beh\xE4lt sich die iReparatur.ch / remarket.ch GmbH das Recht vor, Zahlungsarten ohne Angabe von Gr\xFCnden auszuschliessen.'
      )
    ),
    _react2.default.createElement(
      'h2',
      null,
      '\xA711 Regelungen zu Credits'
    ),
    _react2.default.createElement(
      'ul',
      null,
      _react2.default.createElement(
        'li',
        null,
        _react2.default.createElement(
          'strong',
          null,
          'Remarket-Guthaben (sogenannte Credits)'
        )
      ),
      _react2.default.createElement(
        'li',
        null,
        '1. Guthaben kann nicht zur Begleichung von Kreditkarten Rechnung verwendet oder bar ausbezahlt werden.'
      ),
      _react2.default.createElement(
        'li',
        null,
        '2. Guthaben kann vom Kunden statt einer Bank\xFCberweisung im Rahmen des Verkaufs von Ger\xE4ten erworben werden.'
      ),
      _react2.default.createElement(
        'li',
        null,
        '3. Guthaben/Credits wird auf der Seite remarket.ch als offizielles Zahlungsmittel anerkannt.'
      )
    ),
    _react2.default.createElement(
      'h2',
      null,
      '\xA712 Verkauf von Produkten durch Remarket an den Kunden (Verk\xE4ufe)'
    ),
    _react2.default.createElement(
      'p',
      null,
      _react2.default.createElement(
        'strong',
        null,
        'I. Zustandekommen und Ablauf von Vertr\xE4gen; Eigentumsvorbehalt; Verzug'
      )
    ),
    _react2.default.createElement(
      'ul',
      null,
      _react2.default.createElement(
        'ol',
        null,
        '1. Bei der Darstellung von Produkten in dem Online-Shop des Anbieters handelt es sich nicht um rechtlich bindende Angebote, sondern nur um einen unverbindlichen Online-Katalog.'
      ),
      _react2.default.createElement(
        'ol',
        null,
        '2. Wenn der Kunde auf den Seiten des Anbieters ein oder mehrere Waren ausw\xE4hlt, dem virtuellen Warenkorb hinzuf\xFCgt, den weiteren Prozess der Bestellung durchl\xE4uft und im Anschluss auf die entsprechend gekennzeichnete Schaltfl\xE4che "Bestellung senden" klickt, wird dadurch eine verbindliche Bestellung in Form eines Kaufangebots \xFCber die im Warenkorb enthaltenen Produkte abgegeben. Dabei ist zu beachten, dass Waren im virtuellen Warenkorb nicht einer Reservierung gleichen.'
      ),
      _react2.default.createElement(
        'ol',
        null,
        '3. Auf der rechten Seite des Warenkorbes wird eine Zusammenstellung der gew\xE4hlten Produkte und des zu zahlenden Endpreises einschliesslich der f\xE4lligen Mehrwertsteuer sowie der Versandkosten und Zusatzkosten angezeigt.'
      ),
      _react2.default.createElement(
        'ol',
        null,
        '4. Bevor die Bestellung abgesendet wird, hat der Kunde die M\xF6glichkeit, seine Bestellung noch einmal zu pr\xFCfen und gegebenenfalls durch Anw\xE4hlen der entsprechend beschrifteten Schaltfl\xE4chen zu ver\xE4ndern oder vollst\xE4ndig zu l\xF6schen.'
      ),
      _react2.default.createElement(
        'ol',
        null,
        '5. Nachdem die Bestellung abgesendet wurde, best\xE4tigt der Anbieter deren Eingang per E-Mail. Diese E-Mail enth\xE4lt sowohl den Inhalt der Bestellung als auch s\xE4mtliche Vertragsinformationen. Die E-Mail (Bestellbest\xE4tigung) entspricht der Annahme des Kauf-Angebots durch den Anbieter.'
      )
    ),
    _react2.default.createElement(
      'p',
      null,
      _react2.default.createElement(
        'strong',
        null,
        'II. Vertragsgegenstand und Beschaffenheit'
      )
    ),
    _react2.default.createElement(
      'ul',
      null,
      _react2.default.createElement(
        'ol',
        null,
        '1. Die vom Kunden w\xE4hrend der Bestellung zum Warenkorb hinzugef\xFCgten, anschliessend bestellten und in der Bestellbest\xE4tigung und Auftragsbest\xE4tigung enthaltenen Produkte zu den jeweils genannten Endpreisen stellen den Vertragsgegenstand dar.'
      ),
      _react2.default.createElement(
        'ol',
        null,
        '2. Bei den durch Remarket angebotenen Produkten handelt es sich in der Regel um gebrauchte Ger\xE4te. Dies trifft nicht zu, wenn im Warenkatalog in der entsprechenden Produktbeschreibung ausdr\xFCcklich etwas anderes angegeben wird.'
      ),
      _react2.default.createElement(
        'ol',
        null,
        '3. Der Anbieter weist darauf hin, dass gebrauchte Ware Gebrauchsspuren (wie etwa Verschmutzungen, die aus dem vorherigen Gebrauch resultieren, geringf\xFCgige M\xE4ngel, die den Gebrauch nicht oder nur geringf\xFCgig einschr\xE4nken, Defekte oder Einschr\xE4nkungen der Funktionst\xFCchtigkeit und \xE4hnliche Einschr\xE4nkungen, die f\xFCr gebrauchte Ware typisch sind) aufweisen kann. Diese Gebrauchsspuren werden in den jeweiligen Beschreibungen der Waren im Katalog angegeben. Es wird daher darum gebeten, die Warenbeschreibung sorgf\xE4ltig zu lesen, um Missverst\xE4ndnissen vorzubeugen.'
      ),
      _react2.default.createElement(
        'ol',
        null,
        '4. Es ist zu ber\xFCcksichtigen, dass die Abbildungen auf der Internetseite des Anbieters sowie in dem Katalog die angebotenen Ger\xE4te beziehungsweise Waren unter Umst\xE4nden nur ungenau wiedergeben. Dies gilt insbesondere f\xFCr Farben, die sich aus technischen Gr\xFCnden deutlich unterscheiden k\xF6nnen. Die gezeigten Bilder sind nur als Anschauungsmaterial zu verstehen und k\xF6nnen vom Ger\xE4t abweichen. Teilweise werden anstelle von Abbildungen des konkreten Produkts Symbolbilder oder Typabbildungen gezeigt. Weitere Angaben wie zum Beispiel Gewichts-, Mass- und Leistungsbeschreibungen, Zeichnungen, Abbildungen und technische Daten sind \xFCbliche N\xE4herungswerte.'
      ),
      _react2.default.createElement(
        'ol',
        null,
        '5. Der Anbieter weist darauf hin, dass technische Ger\xE4te erst dann in Betrieb genommen werden sollten, wenn sie die jeweilige Umgebungsraumtemperatur vollst\xE4ndig erreicht haben. Dies ist insbesondere zu beachten, da es w\xE4hrend des Transports zu einer Abk\xFChlung der Ware kommen kann, welche vor der ersten Inbetriebnahme auszugleichen ist.'
      )
    ),
    _react2.default.createElement(
      'h2',
      null,
      '\xA713 Preise und Zahlungsbedingungen'
    ),
    _react2.default.createElement(
      'p',
      null,
      'Lieferung; Versandkosten; Bestellung und Versand von Artikeln ohne Jugendfreigabe; Transportgefahr/-risiko, Aufrechnung'
    ),
    _react2.default.createElement(
      'ul',
      null,
      _react2.default.createElement(
        'ol',
        null,
        '1. Der Anbieter versendet die bestellte Ware, sobald diese verf\xFCgbar ist und soweit mit dem Kunden nicht anders vereinbart wurde, unverz\xFCglich (wenn eine der Zahlungsarten Kreditkarte, Vorauskasse, Paypal oder Sofort\xFCberweisung gew\xE4hlt wurde: unverz\xFCglich bei Zahlungseingang) an die von dem Kunden w\xE4hrend der Bestellung angegebenen Adresse oder alternativ an die angegebene Packstation.'
      ),
      _react2.default.createElement(
        'ol',
        null,
        '2. Wenn eine Bestellung von Artikeln ohne Jugendfreigabe Teil des Auftrags ist, kann die Lieferung an eine Packstation leider nicht durchgef\xFChrt werden.'
      ),
      _react2.default.createElement(
        'ol',
        null,
        '3. Die Lieferzeit betr\xE4gt in der Regel 1-5 Werktage und h\xE4ngt insbesondere von der Bearbeitung des beauftragten Versandunternehmen ab. Die Lieferzeit beginnt ab Zahlungseingang, wenn beim Angebot nichts anderes angeben wurde.'
      ),
      _react2.default.createElement(
        'ol',
        null,
        '4. Wenn eine Lieferung an den Kunden nicht m\xF6glich ist (beispielsweise wenn die zu liefernde Ware nicht durch Haus- oder Eingangst\xFCr oder den Treppenaufgang passt oder der Kunde nicht unter der angegebenen Lieferadresse aufgefunden wird), so hat der Kunde die dadurch entstehenden Kosten zu tragen.'
      ),
      _react2.default.createElement(
        'ol',
        null,
        '5. F\xFCr den Fall, dass die Zustellung der Bestellung durch das Verschulden des Kunden auch beim zweiten Zustellungsversuch scheitert, erkl\xE4rt der Anbieter bereits jetzt den R\xFCcktritt vom Vertrag. Die gegebenenfalls vom Kunden geleisteten Zahlungen werden in diesem Fall unverz\xFCglich vom Anbieter erstattet, wobei die angefallenen Versandkosten von dem Betrag abgezogen werden. Der Anbieter beh\xE4lt sich vor, die Versandkosten zum Gegenstand einer Aufrechnung zu machen.'
      ),
      _react2.default.createElement(
        'ol',
        null,
        '6. Die iReparatur.ch / remarket.ch GmbH hat ein Interesse daran, dass der Kunde den Rechnungsbetrag in einem angemessenen Zeitraum begleicht. Der Anbieter beh\xE4lt sich daher das Recht vor, eine erste Zahlungerinnerung zu schicken und nach 2 Tagen eine letzte Frist von 3 Tagen einzur\xE4umen. Wenn diese letzte Frist ohne Reaktion des Kunden abl\xE4uft, hat die iReparatur.ch / remarket.ch GmbH das Recht, die Bestellung zu annullieren.'
      ),
      _react2.default.createElement(
        'ol',
        null,
        '7.  Bei der Post nicht abgeholte Pakete, welche an uns zur\xFCckgesendet werden (Annahmeverweigerung, Empf\xE4nger nicht ermittelbar, nicht bei der Post abgeholt), werden mit einer Bearbeitungsgeb\xFChr von bis zu 50.00 CHF f\xFCr die Aufwandsentsch\xE4digung (Handling und Versandkosten) verrechnet.'
      )
    ),
    _react2.default.createElement(
      'h2',
      null,
      '\xA714 Widerrufsrecht bei Vertr\xE4gen mit Verbrauchern'
    ),
    _react2.default.createElement(
      'p',
      null,
      'Wenn Ihr Kauf weder einem gewerblichen noch einem der Selbstst\xE4ndigkeit zuzuordnen Zweck dient, so ergibt sich f\xFCr Sie das im Folgenden beschrieben Widerrufsrecht.'
    ),
    _react2.default.createElement(
      'ul',
      null,
      _react2.default.createElement(
        'ol',
        null,
        '1. Bevor Datentr\xE4ger oder Ger\xE4te mit internen Datentr\xE4gern (wie zum Beispiel interne oder externe Festplatten oder andere Massenspeicherger\xE4te sowie USB-Sticks oder Speicherkarten) zur\xFCckgesendet werden, ist eine vorherige Sicherung sowie eine anschliessende L\xF6schung der darauf gespeicherten Daten sicherzustellen. Wir sind vor einem Weiterverkauf der Ger\xE4te weder f\xFCr eine Sicherung der Daten noch f\xFCr eine L\xF6schung verantwortlich.'
      ),
      _react2.default.createElement(
        'ol',
        null,
        '2. Um die Ware sicher zur\xFCckzusenden, sollte m\xF6glichst die Originalverpackung des Ger\xE4ts genutzt werden. Dar\xFCber hinaus sollte gegebenenfalls eine sch\xFCtzende Verpackung verwendet werden. Wenn Sie die originale Verpackung des Ger\xE4ts nicht mehr zur Hand haben, bitten wir Sie, mit einer geeigneten Verpackung den hinreichenden Schutz der Ger\xE4te vor Transportsch\xE4den sicherzustellen und somit etwaige Schadenersatzanspr\xFCche aufgrund von Besch\xE4digungen als Folge unzureichender Verpackung zu vermeiden. Bei Retoure-Sendungen ist darauf zu achten, dass alle Ger\xE4te inklusive der originalen Zubeh\xF6rteile retourniert werden. Wenn Zubeh\xF6rteile nicht im Sendungsumfang enthalten sind, wird der R\xFCckzahlungsbetrag entsprechend reduziert. Diese Regelung tritt auch in Kraft, wenn Original-Zubeh\xF6r ohne das entsprechende Siegel retourniert wird.'
      ),
      _react2.default.createElement(
        'ol',
        null,
        '3. Um den Kaufpreis des Ger\xE4ts zu erstatten, nutzen wir das Zahlungsmittel, das Ihrerseits bei der urspr\xFCnglichen Bestellung verwendet wurde. Wenn bei der Transaktion ein Gutschein verwendet wurde, so reduziert sich der zu erstattende Preis um den auf das jeweilige Produkt angerechneten Wert des Gutscheins.'
      ),
      _react2.default.createElement(
        'ol',
        null,
        '4. Das Gutschreiben der R\xFCckzahlung auf Ihrem Konto erfolgt \u2013 je nach Bank und Land \u2013 etwa 2 bis 7 Tage nach unserer Anweisung. Wenn die Zahlung auf ein Konto im Ausland get\xE4tigt wird, fallen gegebenenfalls Kosten an, die Sie tragen. Etwaige Kosten einer konkreten \xDCberweisung k\xF6nnen Sie bei Ihrem Bankinstitut erfragen.'
      )
    ),
    _react2.default.createElement(
      'h2',
      null,
      '\xA715 Gew\xE4hrleistung'
    ),
    _react2.default.createElement(
      'p',
      null,
      'Gew\xE4hrleistung, Verj\xE4hrung und Haftungsbeschr\xE4nkung, Sicherung /L\xF6schung von Daten, Erfordernis einer M\xE4ngelanzeige (gilt nur bei Unternehmern)'
    ),
    _react2.default.createElement(
      'p',
      null,
      '1. F\xFCr Verbraucher gilt - entsprechend der gesetzlichen Bestimmungen - eine Gew\xE4hrleistungsfrist von 24 Monaten f\xFCr den Kauf neuer Waren. Auf gebrauchte Waren bieten wir eine Gew\xE4hrleistung von 12 Monaten. Diese Gew\xE4hrleistungsfrist beginnt mit der Ablieferung der Ware.'
    ),
    _react2.default.createElement(
      'h2',
      null,
      '\xA716 Mehrwertsteuerr\xFCckerstattung bei Wohnsitz im Ausland'
    ),
    _react2.default.createElement(
      'p',
      null,
      'Die iReparatur.ch / remarket.ch GmbH ist nicht f\xFCr die Erstattung der Mehrwertsteuer im Falle einer privaten Ausfuhr von Waren in das Ausland zust\xE4ndig.'
    ),
    _react2.default.createElement(
      'ul',
      null,
      _react2.default.createElement(
        'li',
        null,
        '1. Beim Akzeptieren der AGB wird ein Vertrag zwischen dem Kunden und der iReparatur.ch / remarket.ch GmbH geschlossen.'
      ),
      _react2.default.createElement(
        'li',
        null,
        '2. Bis zum Eingang der vollst\xE4ndigen Bezahlung ist die Ware im Eigentum der iReparatur.ch / remarket.ch GmbH.'
      ),
      _react2.default.createElement(
        'li',
        null,
        '2a. Wenn der Kunde mit der Zahlung in Verzug ger\xE4t, so hat die iReparatur.ch / remarket.ch GmbH das Recht, von dem Vertrag zur\xFCckzutreten. Im Zuge dieses R\xFCcktritts erlischt f\xFCr den Kunden auch der Anspruch auf die Ware.'
      ),
      _react2.default.createElement(
        'li',
        null,
        '3. Die Angaben zu den Produkten auf der Internetseite auf remarket.ch sowie auch bei schriftlichen Angeboten sind als unverbindlich zu betrachten.'
      ),
      _react2.default.createElement(
        'li',
        null,
        '3a. Der Anbieter beh\xE4lt sich Fehler bei der Beschreibung, Abbildung oder Preisangabe vor.'
      ),
      _react2.default.createElement(
        'li',
        null,
        '3b. Der Anbieter - die iReparatur.ch / remarket.ch GmbH - kann die Verarbeitung der Bestellung zu jedem Zeitpunkt und ohne Angabe eines Grundes abbrechen. Er hat jedoch die Pflicht, diesen Abbruch dem Kunden mitzuteilen.'
      ),
      _react2.default.createElement(
        'li',
        null,
        '4. Die Preise f\xFCr Ger\xE4te werden regelm\xE4ssig an den Markt angepasst. Die iReparatur.ch / remarket.ch GmbH beh\xE4lt sich aus diesem Grund das Recht vor, ihre Preise zu jedem Zeitpunkt zu \xE4ndern.'
      )
    ),
    _react2.default.createElement(
      'h3',
      null,
      'L\xE4nderabh\xE4ngige Regelungen:'
    ),
    _react2.default.createElement(
      'p',
      null,
      _react2.default.createElement(
        'strong',
        null,
        'Schweiz / Liechtenstein:'
      )
    ),
    _react2.default.createElement(
      'p',
      null,
      'Die Preise werden jeweils in Schweizer Franken und inklusive der g\xFCltigen Mehrwertsteuer angegeben.'
    ),
    _react2.default.createElement(
      'ul',
      null,
      _react2.default.createElement(
        'li',
        null,
        '1. Eine Lieferung erfolgt nur innerhalb der Schweiz sowie nach Liechtenstein.'
      ),
      _react2.default.createElement(
        'li',
        null,
        '2. Der Anbieter \xFCbernimmt f\xFCr alle Angaben zu Lieferzeiten keine Gew\xE4hr.'
      ),
      _react2.default.createElement(
        'li',
        null,
        '3. Grunds\xE4tzlich tr\xE4gt der Kunde bei der Lieferung  - auch wenn es sich um Teillieferungen handelt \u2013 das Risiko. Handelt es sich um offensichtliche Transportsch\xE4den, sucht der Anbieter jedoch eine f\xFCr beide Parteien zufriedenstellende L\xF6sung.'
      ),
      _react2.default.createElement(
        'li',
        null,
        '4. Wenn eine Zustellung der bestellten Ware durch ein Verschulden seitens des Kunden scheitert, so hat die iReparatur.ch / remarket.ch GmbH das Recht, vom Vertrag zur\xFCckzutreten. In diesem Fall werden bereits \xFCberwiesene Zahlungen zur\xFCckerstattet.'
      ),
      _react2.default.createElement(
        'li',
        null,
        '5. Wenn ein Ger\xE4t bzw. ein Produkt mit einem offensichtlichen Transportsch\xE4den geliefert wird, so ist der Fehler m\xF6glichst sofort beim Zusteller zu reklamieren. Dar\xFCber hinaus bittet der Anbieter um eine unverz\xFCgliche Kontaktaufnahme. F\xFCr gesetzliche Anspr\xFCche und die damit verbundene Durchsetzung (besonders die Gew\xE4hrleistungsrechte) hat das Vers\xE4umen der Reklamation oder der Kontaktaufnahme keine Konsequenzen. Der Kunde hilft dem Anbieter dadurch aber, die eigenen Anspr\xFCche gegen\xFCber dem Frachtf\xFChrer beziehungsweise gegen\xFCber der Transportversicherung geltend zu machen.'
      ),
      _react2.default.createElement(
        'li',
        null,
        '6. Bei einem Garantiefall hat der Kunde die Kosten f\xFCr den Versand an die iReparatur.ch / remarket.ch GmbH zu tragen.'
      ),
      _react2.default.createElement(
        'li',
        null,
        '7. Wenn Ware mangelhaft ist, wird sie vom Anbieter bis zum Ablauf der Garantiefrist kostenfrei repariert oder ersetzt. Wenn ein Ger\xE4t nicht mehr repariert werden kann, erfolgt der Ersatz durch ein gleichwertiges Ger\xE4t. Falls dies nicht m\xF6glich ist, erstattet der Anbieter den bezahlten Betrag bis zum maximalen Kaufpreis.'
      ),
      _react2.default.createElement(
        'li',
        null,
        '8. Tritt ein Garantiefall auf, so hat dies keine Auswirkung auf die Garantiezeit. F\xFCr die Garantiezeit ist ausschliesslich das Datum der Rechnung beim Kauf ausschlaggebend. Bei einem Austauschger\xE4t beginnt die Garantiezeit entsprechend nicht erneut und verl\xE4ngert sich nicht. Die gleiche Regelung findet bei einer Reparatur unter Garantie Anwendung.'
      ),
      _react2.default.createElement(
        'li',
        null,
        '9. Wenn der Kunde den AGB zustimmt, erkl\xE4rt er sich mit den folgenden Regelungen einverstanden:'
      ),
      _react2.default.createElement(
        'li',
        null,
        '- dass s\xE4mtliche vom Kunden vorgenommenen Reparaturen oder \xC4nderungen ohne schriftliche oder m\xFCndliche Zustimmung der iReparatur.ch / remarket.ch GmbH mit einer Aufhebung der Garantie verbunden sind'
      ),
      _react2.default.createElement(
        'li',
        null,
        '- dass s\xE4mtliche Sch\xE4den durch Dritteinwirkung (wie zum Beispiel Fallsch\xE4den und Fl\xFCssigkeitssch\xE4den) nicht von der Garantie der iReparatur.ch / remarket.ch GmbH gedeckt werden.'
      ),
      _react2.default.createElement(
        'li',
        null,
        '- In einem Garantiefall ist der Kunde selbst daf\xFCr verantwortlich, das Ger\xE4t im Rahmen der Garantiezeit zu einer der Gesch\xE4ftsstellen von iReparatur.ch / remarket.ch GmbH zu senden oder zu bringen. Wenn dies f\xFCr den Kunden nicht m\xF6glich ist, kann der Anbieter Remarket den Anspruch auf Garantie nicht gew\xE4hrleisten.'
      ),
      _react2.default.createElement(
        'li',
        null,
        '- Der Umfang der \u201ERemarket-Garantie\u201C: Abgesehen der bereits genannten gesetzlichen Gew\xE4hrleistung bietet die iReparatur.ch / remarket.ch GmbH seinen Kunden bei Bestellungen \xFCber die Plattform Remarket.ch eine 12-monatige \u201ERemarket -Garantie" (wenn nicht anders angegeben), die f\xFCr elektronische Artikel gilt: Diese Garantie deckt s\xE4mtliche Funktionen der gekauften Ware ab, welche bei der Auslieferung aus dem Werk vom Hersteller als technische Spezifikationen definiert sind. Wenn diese Funktionen innerhalb der 12-monatigen Garantiezeit fehlerhaft sind, hat der Kunde das Recht, das Produkt an den Anbieter zur\xFCckzuschicken. Um die Ware kostenlos zur\xFCckzusenden, kann der Kunde den Frankierschein im Retourenportal, unter dem Men\xFCpunkt \u201EMein Konto\u201C nutzen. Dabei ist zu beachten, dass das Ger\xE4t inklusive der originalen Zubeh\xF6rteile zur\xFCckgesendet werden muss. Wenn diese Zubeh\xF6rteile nicht Teil der Sendung sind, wird der m\xF6gliche R\xFCckzahlungsbetrag um den Wert der Zubeh\xF6rteile vermindert.'
      ),
      _react2.default.createElement(
        'li',
        null,
        '- Ab dem Tag der Ablieferung beim Kunden betr\xE4gt die Garantiezeit 12 Monate (wenn nicht anders angegeben). Eine Verl\xE4ngerung dieser Garantiezeit (beispielsweise aufgrund der Vornahme von Garantieleistungen) ist nicht vorgesehen. Die Garantiezeit f\xE4ngt in solchen F\xE4llen auch nicht erneut an zu laufen.'
      ),
      _react2.default.createElement(
        'li',
        null,
        '- Nachdem der Kunde \u2013 sofern er der Verbraucher ist - die bestellten Produkte zur\xFCckgesendet hat, pr\xFCft der Anbieter die Produkte auf M\xE4ngel im Sinne der oben genannten Funktionsm\xE4ngel. Werden dabei M\xE4ngel festgestellt, so steht es dem Anbieter frei, die Ware entweder zu reparieren oder den Wert des Ger\xE4ts zu erstatten. Dabei ist davon auszugehen, dass die Reparatur des alten Ger\xE4tes den Regelfall darstellt. Wenn eine Reparatur nicht m\xF6glich ist, beh\xE4lt der Anbieter das Ger\xE4t und erstattet dem Kunden den gezahlten Kaufpreis. Die Entscheidung f\xFCr eine Reparatur oder eine Erstattung des Kaufpreises liegt dabei im Ermessen des Anbieters und bedarf keiner Begr\xFCndung. Der Anbieter kontaktiert den Kunden in Bezug auf die Warenr\xFCcksendung und tr\xE4gt die Kosten der R\xFCcksendung.'
      ),
      _react2.default.createElement(
        'li',
        null,
        '- Um die R\xFCckzahlung zu t\xE4tigen, verwendet Remarket dasselbe Zahlungsmittel, das der Kunde bei der urspr\xFCnglichen Zahlung eingesetzt hat. Wenn die R\xFCckzahlung per \xDCberweisung auf ein Bankkonto erfolgt, so wird der Betrag dem Bankkonto in Abh\xE4ngigkeit von Bank und Land nach 2 bis 7 Werktagen nach Anweisung gutgeschrieben. Wenn bei einer \xDCberweisung ins Ausland Kosten anfallen, sind diese vom Kunden zu tragen. Die konkreten Kosten daf\xFCr kann der Kunde bei seinem Bankinstitut erfragen.'
      ),
      _react2.default.createElement(
        'li',
        null,
        '- Der Kunde hat dar\xFCber, dass er den Mangel nicht selbst verursacht hat sowie \xFCber den Zeitpunkt des Eintritts des Mangels eine eidesstattliche Erkl\xE4rung gegen\xFCber dem Anbieter abzugeben. Wenn die \u201ERemarket-Garantie" in Ansbruch genommen wird, so hat der Kunde dem Anbiter den Mangel unverz\xFCglich nach der Entstehung mitzuteilen. Eine Erkl\xE4rung in Bezug auf die Inanspruchnahme der \u201ERemarket-Garantie" muss dabei in einem Zeitraum von 18 Monaten nach Datum der Ablieferung beim Anbieter eingehen. Die Erkl\xE4rung ist an die folgende Adresse zu richten: iReparatur.ch / remarket.ch GmbH, Gerbergasse 82, CH-4001 Basel.  Alternativ kann die Erkl\xE4rung per E-Mail an den Kundendienst oder \xFCber das Kontaktformular https://www.remarket.ch/kontakt gesendet werden.'
      ),
      _react2.default.createElement(
        'li',
        null,
        '- Die Remarket-Garantie kann nicht auf Dritte \xFCbertragen werden.'
      ),
      _react2.default.createElement(
        'li',
        null,
        '- Ausschluss der \u201ERemarket-Garantie": Die Remarket-Garantie erstreckt sich nicht auf:'
      )
    ),
    _react2.default.createElement(
      'ul',
      null,
      _react2.default.createElement(
        'li',
        null,
        'a. Verschleissteile wie beispielsweise entfernbare Speicherkarten sowie Verschlussst\xFCcke bei Objektiven und Kameras;'
      ),
      _react2.default.createElement(
        'li',
        null,
        'b. zerbrechliche Teile wie zum Beispiel Glas, Display, Linsen, die zu Bruch gegangen sind;'
      ),
      _react2.default.createElement(
        'li',
        null,
        'c. Verbrauchsteile wie Batterien oder Akkus;'
      ),
      _react2.default.createElement(
        'li',
        null,
        'd. Fehler durch falschen Betrieb oder falsche Bedienung, Sch\xE4den durch aggressive Einfl\xFCsse der Umgebung sowie Chemikalien oder Reinigungsmittel;'
      ),
      _react2.default.createElement(
        'li',
        null,
        'e. M\xE4ngel, die im Rahmen von Installation oder Transport verursacht worden sind;'
      ),
      _react2.default.createElement(
        'li',
        null,
        'f. Medienartikel wie DVDs, Software und weitere'
      ),
      _react2.default.createElement(
        'li',
        null,
        'g. M\xE4ngel, die durch eine unsachgem\xE4sse Behandlung seitens des Kunden oder durch eine \xFCberm\xE4ssige Nutzung, eine falsche Handhabung oder eine falsche Bedienung der Ware entstanden sind. Dazu geh\xF6ren insbesondere auch eine falsche Aufbewahrung des Ger\xE4ts sowie Sch\xE4den, die durch Sturz oder starke Ersch\xFCtterung entstanden sind.'
      ),
      _react2.default.createElement(
        'li',
        null,
        'h. Ein etwaiger Siegelbruch, der nicht entsprechend der Bedienungsanleitung erforderlich ist;'
      ),
      _react2.default.createElement(
        'li',
        null,
        'i. Gebrochenes Display;'
      ),
      _react2.default.createElement(
        'li',
        null,
        'j. Gebrochenes Geh\xE4use;'
      ),
      _react2.default.createElement(
        'li',
        null,
        'k. Kabelbruch, wenn das Kopfh\xF6rerkabel nicht austauschbar ist;'
      ),
      _react2.default.createElement(
        'li',
        null,
        'l. Wasserschaden;'
      ),
      _react2.default.createElement(
        'li',
        null,
        'm. Elektrische Defekte, die aus der Verwendung von nicht durch den Hersteller zugelassenen Ladeger\xE4ten resultieren;'
      ),
      _react2.default.createElement(
        'li',
        null,
        'n. Sch\xE4den durch die Installation von Software, die nicht durch den Hersteller zugelassen ist (einschliesslich Apps und weitere);'
      ),
      _react2.default.createElement(
        'li',
        null,
        'o. Sch\xE4den durch ein Nichteinhalten der Montage-, Pflege- und Gebrauchsanleitung seitens des Kunden'
      ),
      _react2.default.createElement(
        'li',
        null,
        'p. Sch\xE4den, die sich auf eine normale Abnutzung \u2013 wie zum Beispiel Gebrauchsspuren am Geh\xE4use oder am Display  oder eine vors\xE4tzliche Besch\xE4digung zur\xFCckf\xFChren lassen.'
      ),
      _react2.default.createElement(
        'li',
        null,
        'q. Sch\xE4den durch eine unsachgem\xE4sse Inbetriebnahme'
      ),
      _react2.default.createElement(
        'li',
        null,
        'r. Sch\xE4den durch eine mangelnde oder fehlerhafte Wartung'
      ),
      _react2.default.createElement(
        'li',
        null,
        's. Sch\xE4den am Ger\xE4t, die auf eine Verwendung schliessen lassen, die nicht dem vorgesehenen Zweck entspricht'
      ),
      _react2.default.createElement(
        'li',
        null,
        't. Sch\xE4den durch Ungl\xFCcke, Naturkatastrophen und alle anderen Gr\xFCnde, die vom Anbieter weder kontrolliert noch vorauszusehen sind. Diese Ursachen schliessen ein, sind aber nicht begrenzt auf Blitzschlag, Wasser, Feuer, Aufruhr und unzureichende L\xFCftungs- und Klimaverh\xE4ltnisse'
      )
    ),
    _react2.default.createElement(
      'h2',
      null,
      '\xA717 Gew\xE4hrleistung und Freiwillige R\xFCcknahmegarantie f\xFCr Verbraucher'
    ),
    _react2.default.createElement(
      'p',
      null,
      'Bei M\xE4ngeln an der gelieferten Ware hat der Kunde zun\xE4chst lediglich einen Anspruch auf die Nacherf\xFCllung. Das bedeutet, dass er je nach Wunsch die Nachbesserung der gelieferten Ware oder auch eine Ersatzlieferung vom Anbieter verlangen kann. Dabei hat der Kunde seinen Wunsch dem Anbieter in deutlicher Form mitzuteilen. Nach zwei erfolglosen Versuchen der Nacherf\xFCllung seitens des Anbieters kann der Kunde die zur Verf\xFCgung stehenden gesetzlichen Gew\xE4hrleistungsanspr\xFCche nutzen (z.B.  Minderung des Kaufpreises und R\xFCcktritt vom Vertrag).'
    ),
    _react2.default.createElement(
      'p',
      null,
      'Wenn sich der Kunde im Falle defekter Neuware f\xFCr eine Nachbesserung (Reparatur) entscheidet , so ist das betroffene Ger\xE4t auf Verlangen des Anbieters im Sine einer z\xFCgigen Abwicklung direkt an den Hersteller zu senden. Die Anschrift des Herstellers wird dem Kunden vom Anbieter dabei unverz\xFCglich mitgeteilt. F\xFCr den Fall, dass eine Reparatur \xFCber den Hersteller nicht durchf\xFChrbar ist, so muss der Kunde das Ger\xE4t freigemacht an den Anbieter zur\xFCcksenden. Der Anbieter hat dabei das Recht, die aus der Gew\xE4hrleistung resultierenden Leistungen davon abh\xE4ngig zu machen, dass der Kunde das defekte Ger\xE4t zun\xE4chst an den Anbieter zur\xFCckschickt und die \xDCberpr\xFCfung des Anspruchs erm\xF6glicht. Wenn sich herausstellt, dass trotz gr\xFCndlicher Pr\xFCfung kein Fehler feststellbar ist oder dass der Defekt vom Kunden selbst verursacht wurde, so sind Anspr\xFCche im Rahmen der Gew\xE4hrleistung ausgeschlossen. Der Anbieter sendet dem Kunden das Ger\xE4t in diesem Fall in unver\xE4ndertem Zustand zur\xFCck, es sei denn der Anbieter hat sich vorher per E-Mail, schriftlich oder in Textform bereiterkl\xE4rt, die Reparatur- und Versandkosten zu \xFCbernehmen.'
    ),
    _react2.default.createElement(
      'p',
      null,
      'Wenn in der Zwischenzeit Preissenkungen eingetreten sind, so hat der Anbieter das Recht, das Ger\xE4t zu aktuellen Marktpreisen gutzuschreiben oder alternativ ein gleichwertiges Ger\xE4t zu besorgen. Dar\xFCber hinaus beh\xE4lt sich der Anbieter das Recht vor, den Wert des Ger\xE4ts zu reduzieren, wenn eine Minderung des Wertes durch die Nutzung oder Abnutzung des Kunden vorliegt.'
    ),
    _react2.default.createElement(
      'h2',
      null,
      '\xA718 Eigentumsvorbehalt'
    ),
    _react2.default.createElement(
      'p',
      null,
      'Wenn Vertr\xE4ge mit Verbrauchern vorliegen, ist der Anbieter bis zur vollst\xE4ndigen Bezahlung des Kaufpreises der Eigent\xFCmer der Ware. Liegen Vertr\xE4ge mit Unternehmern vor, so ist der Anbieter bis zur Zahlung aller Forderungen der laufenden Gesch\xE4ftsbeziehung Eigent\xFCmer der Ware. Wenn der Kaufpreis noch nicht komplett bezahlt wurde, so hat der Kunde die Pflicht,'
    ),
    _react2.default.createElement(
      'ul',
      null,
      _react2.default.createElement(
        'li',
        null,
        '- das Ger\xE4t pfleglich zu behandeln'
      ),
      _react2.default.createElement(
        'li',
        null,
        '- einen Zugriff Dritter auf das Ger\xE4t (wie zum Beispiel im Falle einer Pf\xE4ndung), eine gegebenenfalls auftretende Besch\xE4digung des Ger\xE4ts oder eine Vernichtung und/oder'
      ),
      _react2.default.createElement(
        'li',
        null,
        '- einen Wechsel des Besitzes des Ger\xE4ts sowie auch einen eigenen Wechsel des Wohnsitzes dem Anbieter unverz\xFCglich mitzuteilen.'
      )
    ),
    _react2.default.createElement(
      'p',
      null,
      'Der Anbieter hat das Recht, von dem Vertrag zur\xFCckzutreten und die R\xFCcksendung des Ger\xE4ts zu verlangen, wenn ein vertragswidriges Verhalten des Kunden vorliegt. Das gilt insbesondere bei Zahlungsverzug oder bei Verletzung der oben genannten Pflichten. Der Unternehmer hat das Recht, das Ger\xE4t im ordentlichen Gesch\xE4ftsverkehr weiter zu ver\xE4ussern. Er tritt dem Anbieter bereits zu diesem Zeitpunkt s\xE4mtliche Forderungen in H\xF6he des Betrages der Rechnung ab, die dem Unternehmer durch die weitere Ver\xE4usserung gegen\xFCber einem Dritten zukommen. Der Anbieter nimmt diese Abtretung an. Im Anschluss an die Abtretung hat der Unternehmer das Recht, die Forderung einzuziehen. Der Anbieter beh\xE4lt sich das Recht vor, diese Forderung selbst einzuziehen, wenn der Unternehmer den Verpflichtungen zur Zahlung nicht ordnungsgem\xE4ss nachkommt und in Verzug ger\xE4t.'
    ),
    _react2.default.createElement(
      'h2',
      null,
      '\xA719 Haftung / Haftungsausschluss / Haftungsbeschr\xE4nkung'
    ),
    _react2.default.createElement(
      'p',
      null,
      'Die iReparatur.ch / remarket.ch GmbH ist f\xFCr Sch\xE4den haftbar, die durch sie selbst oder ihre Mitarbeiter und/oder ihre Erf\xFCllungsgehilfen grob fahrl\xE4ssig oder vors\xE4tzlich verursacht worden sind. Diese Haftung ist auch bei einem arglistigem Verschweigen von M\xE4ngeln, bei der \xDCbernahme einer Beschaffenheitsgarantie sowie bei Sch\xE4den aus der Verletzung des Lebens, des K\xF6rpers oder der Gesundheit g\xFCltig. Bei sonstigen Sch\xE4den kann die iReparatur.ch / remarket.ch GmbH lediglich haftbar gemacht werden, wenn eine Pflicht verletzt worden ist, deren Erf\xFCllung die ordnungsgem\xE4sse Vertragsdurchf\xFChrung \xFCberhaupt erst erm\xF6glicht hat und auf deren Einhaltung ein Vertragspartner regelm\xE4ssig vertrauen darf und sofern der eingetretene Schaden vertragstypisch und vorhersehbar gewesen ist. Eine \xFCber diese F\xE4lle hinausgehende Haftung wird von der iReparatur.ch / remarket.ch GmbH hiermit ausdr\xFCcklich ausgeschlossen.'
    )
  );
};

AGB.propTypes = {};
AGB.defaultProps = {};

exports.default = AGB;

/***/ })

});